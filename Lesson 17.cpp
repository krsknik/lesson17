﻿#include <iostream>

class Vector 
{
public:
	Vector() : x(5), y(5) 
	{}
	Vector(double _x, double _y) : x(_x), y(_y)
	{}
	
	void Show() 
	{
		std::cout << '\n' << x << "," << y << '\n';
	}

	static double GetLength(Vector vector)
	{
		return (sqrt((vector.x * vector.x) + (vector.y * vector.y)));
	}

private: 
	double x = 0;
	double y = 0;
};

int main()
{
	Vector v;
	v.Show();

	std::cout << "Length: " << Vector::GetLength(v) << std::endl;
}
